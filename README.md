# wheeze _beta_

> Create a timeline of events outlining where the latency is in your app.

#### <https://bitbucket.org/shielsasaurus/wheeze> #

## Table of Contents
1. Introduction
2. Usage
3. Documentation
4. Contributors

# 1. Introduction

There are two reasons why we might want to use __wheeze__. The first, and obvious reason is that it allows us to find and differentiate sources of latency with as little overhead itself (creating as little artificial latency as possible), and therefore more acurate. The second reason is that we can log & monitor whats going on in our app at any point in time with little code.


# 2. Usage

### 2.1 Asynchronous
```javascript
var wheeze = require('wheeze'); // 'include' the library (wheeze) that is installed via NPM

var session = new wheeze.asyncSession(function() { // Code to be monitored goes in the first argument (typeof Function).
	console.log("This is wheeze!");
	console.log("This code is being monitored.");
}, function(result) { // Code called when the code is finished, and the results are returned as a wheezeReport object.	
	console.log(wheeze.generateReport(result)); // The result can be parsed into a text result via generateReport() for you for easier debugging.
});
```

### 2.2 Synchronous
_Support for using wheeze synchronously will be supported soon_

# 3. Documentation
Documentation can be found on the [wiki](https://bitbucket.org/shielsasaurus/wheeze/wiki).

# 4. Contributors
[Luke Shiels](mailto:shielsasaurus@gmail.com)